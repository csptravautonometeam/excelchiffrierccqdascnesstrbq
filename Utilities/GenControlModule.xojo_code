#tag Module
Protected Module GenControlModule
	#tag Constant, Name = CARRIAGERETURN, Type = String, Dynamic = True, Default = \"\r", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"techeol"
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMETEST, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"techeol"
	#tag EndConstant

	#tag Constant, Name = HOSTPHOTOTEST, Type = String, Dynamic = True, Default = \"192.168.1.86", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPREPRODUCTION, Type = String, Dynamic = True, Default = \"preprod.ml6techeol.tech", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPROD, Type = String, Dynamic = True, Default = \"192.168.1.10", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTTEST, Type = String, Dynamic = True, Default = \"192.168.1.86", Scope = Public
	#tag EndConstant

	#tag Constant, Name = LEAVEAPPMESSAGE, Type = String, Dynamic = True, Default = \"Vous \xC3\xAAtes en train de quitter cette application.\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Vous \xC3\xAAtes en train de quitter cette application."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"You are about to leave this application."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vous \xC3\xAAtes en train de quitter cette application."
	#tag EndConstant

	#tag Constant, Name = PASSWORD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"postgres"
	#tag EndConstant

	#tag Constant, Name = USER, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"admin"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule

#tag Window
Begin Window MainWindow
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   534
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   -1109106120
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Extraction de données"
   Visible         =   True
   Width           =   582
   Begin TextArea MainWindowEditField
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   True
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   424
      HelpTag         =   ""
      HideSelection   =   True
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LimitText       =   0
      LineHeight      =   0.0
      LineSpacing     =   1.0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Mask            =   ""
      Multiline       =   True
      ReadOnly        =   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollbarVertical=   True
      Styled          =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   90
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   542
   End
   Begin Label MainWindowStaticText
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   39
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   193
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Multiline       =   True
      Scope           =   0
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Création de la feuille Excel contenant la liste des parts employeur et CNESST à analyser pour la compagnie sélectionnée."
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   6
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   369
   End
   Begin PopupMenu CompagniePopupMenu
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "hlther\r\ntecheol"
      Italic          =   False
      Left            =   20
      ListIndex       =   1
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   10
      Underline       =   False
      Visible         =   True
      Width           =   161
   End
   Begin PopupMenu DatePeriodePaiePopupMenu
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   20
      ListIndex       =   1
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   36
      Underline       =   False
      Visible         =   True
      Width           =   161
   End
   Begin PopupMenu ProjetPopupMenu
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   20
      ListIndex       =   1
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   62
      Underline       =   False
      Visible         =   True
      Width           =   161
   End
   Begin PushButton ProjetPushButton
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Création de la feuille Excel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   193
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   67
      Underline       =   False
      Visible         =   True
      Width           =   161
   End
   Begin PushButton DatePeriodePushButton
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Création de la feuille Excel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   193
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   44
      Underline       =   False
      Visible         =   True
      Width           =   161
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  determineEnvironnement
		  listeDatePeriodePaie(CompagniePopupMenu.Text)
		  listeProjet(CompagniePopupMenu.Text)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Function addTitleCell(titre_param As String) As XLSCell
		  Dim c as New XLSCell(titre_param)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 14
		  c.FillColor = &cFFBB44
		  c.FontColor = &cFF0000
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub createChiffrierCompagnie(compagnie_param As String, selection_param as String)
		  dim w as XLSWorkBook
		  
		  // create a new workbook
		  w = new XLSWorkbook
		  MainWindowEditField.text = ""
		  
		  w.WindowHeightInTwips = 288
		  w.WindowWidthInTwips = 288
		  
		  Dim d as New Date
		  Dim dateToday As String = format(d.Year,"0000") + format(d.month,"00") + format(d.day,"00")
		  
		  // add worksheet to the work book
		  If selection_param = "DatePeriode" Then
		    w.workSheets.append createChiffrierDasCnesstDatePeriode(compagnie_param, selection_param)
		  Else
		    w.workSheets.append createChiffrierDasCnesstProjet(compagnie_param, selection_param)
		  End If
		  
		  dim f as FolderItem 
		  f = GetSaveFolderItem("", "Excel DAS CNESST " + compagnie_param + " " + selection_param + " " + dateToday + ".xml")
		  
		  if not(f  is nil) Then
		    // this will OVERWRITE an existing file if it can
		    // so you should do all the normal check if the file exists, do you want to overwrite kind of checks 
		    // before you call this
		    w.save(f)
		    
		    //f.Launch
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function createChiffrierDasCnesstDatePeriode(compagnie_param As String, selection_param as String) As XLSWorkSheet
		  Dim message,strSQL As String
		  message = ouvertureBD(compagnie_param)
		  
		  Dim ws as XLSWorkSheet
		  
		  // create a new worksheet
		  ws = new XLSWorkSheet
		  
		  // Create title row
		  // add the row to the spreadsheet 
		  ws.AppendRow createTitleDatePeriode
		  
		  // Lecture du réalisé pour les dépenses de main d'oeuvre par projet et employé
		  
		  strSQL = _
		  " SELECT  realisemocont_projet_no, projet.projet_titre, realisemocont_date_periode, realisemocont_date_jour, realisemocont_nas, realisemocont_nom, realisemocont_prenom, realisemocont_salperiode,  " + _
		  "               realisemocont_salbrut, realisemocont_partemployeur, realisemocont_das, realisemocont_cnesst, realisemocont_cnesst_taux  " + _
		  "   FROM   realisemocont" + _
		  "   LEFT JOIN projet ON realisemocont_projet_no = projet.projet_no " + _
		  "   WHERE realisemocont_date_periode = '" + DatePeriodePaiePopupMenu.Text + "' " + _ 
		  "    ORDER BY  realisemocont_nas, realisemocont_date_jour"
		  
		  //"    ORDER BY  realisemocont_projet_no, realisemocont_date_periode, realisemocont_date_jour"
		  
		  Dim projetRS As RecordSet = bdGop.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  Dim index As Integer = 1
		  Dim nas_sav, nasTrav, formule As String = ""
		  Dim salTotTrav, dasTotTrav, cnesstTotTrav, cnesstTauxTrav As Double = 0
		  Dim r as XLSRow
		  Dim c as XLSCell
		  projetRS.MoveFirst
		  nas_sav = projetRS.Field("realisemocont_nas").StringValue
		  While Not  projetRS.EOF
		    
		    r  =  new XLSRow
		    index = index + 1
		    nasTrav = projetRS.Field("realisemocont_nas").StringValue
		    If  nasTrav <> nas_sav Then
		      // Lire les retenus de l'employeur
		      Dim retenuDict As Dictionary = retenuExtr(nas_sav, projetRS.Field("realisemocont_date_periode").StringValue)
		      nas_sav = nasTrav
		      MainWindowEditField.text = MainWindowEditField.text + "Nas " + nasTrav + " Traité" + chr(13)
		      For i As Integer = 1 To 9
		        c = createStdText("")
		        r.AppendCell c
		      Next i
		      c = createStdNumber(salTotTrav)
		      r.AppendCell c
		      c = createStdNumber(dasTotTrav)
		      r.AppendCell c
		      c = createStdNumber(cnesstTotTrav)
		      r.AppendCell c
		      c = createStdNumber(cnesstTauxTrav)
		      r.AppendCell c
		      formule = "=RC[-4]+(RC[-3]+RC[-2])"
		      c = createStdFormula(formule)
		      r.AppendCell c
		      c = createStdNumber(retenuDict.Lookup("Applicable", 0))
		      r.AppendCell c
		      c = createStdNumber(retenuDict.Lookup("PartEmployeur", 0))
		      r.AppendCell c
		      c = createStdNumber(retenuDict.Lookup("AvImPosFed", 0))
		      r.AppendCell c
		      
		      salTotTrav = 0
		      dasTotTrav = 0
		      cnesstTotTrav = 0
		      cnesstTauxTrav = 0
		      ws.AppendRow r
		      Continue
		    End If
		    
		    c = createStdText(compagnie_param)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_projet_no").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_titre").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_date_periode").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_date_jour").StringValue)
		    r.AppendCell c
		    c = createStdText(nasTrav)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_nom").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_prenom").StringValue)
		    r.AppendCell c
		    
		    // Lecture des données de retenues
		    
		    c = createStdNumber(projetRS.Field("realisemocont_salperiode").DoubleValue)
		    r.AppendCell c
		    salTotTrav = salTotTrav + projetRS.Field("realisemocont_salbrut").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_salbrut").DoubleValue)
		    r.AppendCell c
		    dasTotTrav = dasTotTrav + projetRS.Field("realisemocont_das").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_das").DoubleValue)
		    r.AppendCell c
		    cnesstTotTrav = cnesstTotTrav + projetRS.Field("realisemocont_cnesst").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_cnesst").DoubleValue)
		    r.AppendCell c
		    cnesstTauxTrav = projetRS.Field("realisemocont_cnesst_taux").DoubleValue
		    
		    // Dépenses CMEQ
		    //MontantDouble1 = estimeDict.Lookup("ReelMontantTotalMO",0) + estimeDict.Lookup("ReelMontantTotalMAT",0)
		    //c = createStdNumber( MontantDouble1)
		    //r.AppendCell c
		    
		    // Marge brute calculée
		    //formule = "=RC[-1]-(RC[-3]+RC[-2])"
		    //c = createStdFormula(formule)
		    //r.AppendCell c
		    
		    Suivant:
		    ws.AppendRow r
		    projetRS.MoveNext
		  Wend
		  
		  // Écrire dernière ligne
		  Dim retenuDict As Dictionary = retenuExtr(nas_sav, projetRS.Field("realisemocont_date_periode").StringValue)
		  MainWindowEditField.text = MainWindowEditField.text + "Nas " + nasTrav + " Traité" + chr(13)
		  For i As Integer = 1 To 9
		    c = createStdText("")
		    r.AppendCell c
		  Next i
		  c = createStdNumber(salTotTrav)
		  r.AppendCell c
		  c = createStdNumber(dasTotTrav)
		  r.AppendCell c
		  c = createStdNumber(cnesstTotTrav)
		  r.AppendCell c
		  c = createStdNumber(cnesstTauxTrav)
		  r.AppendCell c
		  formule = "=RC[-4]+(RC[-3]+RC[-2])"
		  c = createStdFormula(formule)
		  r.AppendCell c
		  c = createStdNumber(retenuDict.Lookup("Applicable", 0))
		  r.AppendCell c
		  c = createStdNumber(retenuDict.Lookup("PartEmployeur", 0))
		  r.AppendCell c
		  c = createStdNumber(retenuDict.Lookup("AvImPosFed", 0))
		  r.AppendCell c
		  projetRS.Close
		  projetRS = Nil
		  
		  bdGOP.Close
		  bdGop = Nil
		  
		  return ws
		  
		  //" SELECT  realisemocont_projet_no, projet.projet_titre, realisemocont_date_periode, realisemocont_date_jour, realisemocont_nas, realisemocont_nom, realisemocont_prenom, realisemocont_salperiode,  " + _
		  //"               realisemocont_salbrut, realisemocont_partemployeur, realisemocont_das, realisemocont_cnesst  " + _
		  //"   FROM   realisemocont" + _
		  //"   LEFT JOIN projet ON realisemocont_projet_no = projet.projet_no " + _
		  //"   WHERE realisemocont_date_periode = '" + DatePeriodePaiePopupMenu.Text + "' " + _ 
		  //"    ORDER BY  realisemocont_nas, realisemocont_date_jour"
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function createChiffrierDasCnesstProjet(compagnie_param As String, selection_param as String) As XLSWorkSheet
		  Dim message,strSQL As String
		  message = ouvertureBD(compagnie_param)
		  
		  Dim ws as XLSWorkSheet
		  
		  // create a new worksheet
		  ws = new XLSWorkSheet
		  
		  // Create title row
		  // add the row to the spreadsheet 
		  ws.AppendRow createTitleProjet
		  
		  // Lecture du réalisé pour les dépenses de main d'oeuvre par projet et employé
		  strSQL = _
		  " SELECT  realisemocont_projet_no, projet.projet_titre, realisemocont_date_periode, realisemocont_date_jour, realisemocont_nas, realisemocont_nom, realisemocont_prenom, realisemocont_salperiode,  " + _
		  "               realisemocont_salbrut, realisemocont_partemployeur, realisemocont_das, realisemocont_cnesst, realisemocont_cnesst_taux  " + _
		  "   FROM   realisemocont" + _
		  "   LEFT JOIN projet ON realisemocont_projet_no = projet.projet_no " + _
		  "   WHERE realisemocont_projet_no = '" + ProjetPopupMenu.Text + "' " + _ 
		  "    ORDER BY  realisemocont_date_periode, realisemocont_date_jour, realisemocont_nas"
		  //"    ORDER BY  realisemocont_projet_no, realisemocont_date_periode, realisemocont_date_jour"
		  
		  Dim projetRS As RecordSet = bdGop.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  Dim index As Integer = 1
		  Dim datePeriode_sav, datePeriodeTrav, formule As String = ""
		  Dim salTotTrav, dasTotTrav, cnesstTotTrav, cnesstTauxTrav As Double = 0
		  Dim r as XLSRow
		  Dim c as XLSCell
		  projetRS.MoveFirst
		  datePeriode_sav = projetRS.Field("realisemocont_date_periode").StringValue
		  While Not  projetRS.EOF
		    
		    r =  new XLSRow
		    index = index + 1
		    datePeriodeTrav = projetRS.Field("realisemocont_date_periode").StringValue
		    If  datePeriodeTrav <> datePeriode_sav Then
		      datePeriode_sav = datePeriodeTrav
		      MainWindowEditField.text = MainWindowEditField.text + "Date période " + datePeriodeTrav + " Traité" + chr(13)
		      For i As Integer = 1 To 9
		        c = createStdText("")
		        r.AppendCell c
		      Next i
		      c = createStdNumber(salTotTrav)
		      r.AppendCell c
		      c = createStdNumber(dasTotTrav)
		      r.AppendCell c
		      c = createStdNumber(cnesstTauxTrav)
		      r.AppendCell c
		      formule = "=RC[-3]+(RC[-2]+RC[-1])"
		      c = createStdFormula(formule)
		      r.AppendCell c
		      
		      salTotTrav = 0
		      dasTotTrav = 0
		      cnesstTotTrav = 0
		      cnesstTauxTrav = 0
		      ws.AppendRow r
		      Continue
		    End If
		    
		    c = createStdText(compagnie_param)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_projet_no").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_titre").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_date_periode").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_date_jour").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_nas").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_nom").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("realisemocont_prenom").StringValue)
		    r.AppendCell c
		    
		    // Lecture des données de retenues
		    
		    c = createStdNumber(projetRS.Field("realisemocont_salperiode").DoubleValue)
		    r.AppendCell c
		    salTotTrav = salTotTrav + projetRS.Field("realisemocont_salbrut").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_salbrut").DoubleValue)
		    r.AppendCell c
		    dasTotTrav = dasTotTrav + projetRS.Field("realisemocont_das").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_das").DoubleValue)
		    r.AppendCell c
		    cnesstTotTrav = cnesstTotTrav + projetRS.Field("realisemocont_cnesst").DoubleValue
		    c = createStdNumber(projetRS.Field("realisemocont_cnesst").DoubleValue)
		    r.AppendCell c
		    
		    // Dépenses CMEQ
		    //MontantDouble1 = estimeDict.Lookup("ReelMontantTotalMO",0) + estimeDict.Lookup("ReelMontantTotalMAT",0)
		    //c = createStdNumber( MontantDouble1)
		    //r.AppendCell c
		    
		    // Marge brute calculée
		    //formule = "=RC[-1]-(RC[-3]+RC[-2])"
		    //c = createStdFormula(formule)
		    //r.AppendCell c
		    
		    Suivant:
		    ws.AppendRow r
		    projetRS.MoveNext
		  Wend
		  // Écrire dernière ligne
		  MainWindowEditField.text = MainWindowEditField.text + "Date période " + datePeriodeTrav + " Traité" + chr(13)
		  For i As Integer = 1 To 9
		    c = createStdText("")
		    r.AppendCell c
		  Next i
		  c = createStdNumber(salTotTrav)
		  r.AppendCell c
		  c = createStdNumber(dasTotTrav)
		  r.AppendCell c
		  c = createStdNumber(cnesstTauxTrav)
		  r.AppendCell c
		  formule = "=RC[-3]+(RC[-2]+RC[-1])"
		  c = createStdFormula(formule)
		  r.AppendCell c
		  
		  salTotTrav = 0
		  dasTotTrav = 0
		  cnesstTotTrav = 0
		  cnesstTauxTrav = 0
		  ws.AppendRow r
		  projetRS.Close
		  projetRS = Nil
		  
		  bdGOP.Close
		  bdGop = Nil
		  
		  return ws
		  
		  //" SELECT  realisemocont_projet_no, projet.projet_titre, realisemocont_date_periode, realisemocont_date_jour, realisemocont_nas, realisemocont_nom, realisemocont_prenom, realisemocont_salperiode,  " + _
		  //"               realisemocont_salbrut, realisemocont_partemployeur, realisemocont_das, realisemocont_cnesst  " + _
		  //"   FROM   realisemocont" + _
		  //"   LEFT JOIN projet ON realisemocont_projet_no = projet.projet_no " + _
		  //"   WHERE realisemocont_date_periode = '" + DatePeriodePaiePopupMenu.Text + "' " + _ 
		  //"    ORDER BY  realisemocont_nas, realisemocont_date_jour"
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function createStdFormula(formula_param As String) As XLSCell
		  Dim c As XLSCell = New XLSCell
		  // cells can have styles (fill color, fill style, text color, fomulas, vlalues, etc)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 11
		  c.value = "0"
		  c.type = XLSCell.typeNumber
		  c.formula = formula_param
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function createStdNumber(nombreDouble_param As Double) As XLSCell
		  Dim c As XLSCell = New XLSCell
		  // cells can have styles (fill color, fill style, text color, fomulas, vlalues, etc)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 11
		  c.value = str(nombreDouble_param)
		  c.type = XLSCell.typeNumber
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function createStdText(text_param As String) As XLSCell
		  Dim c As XLSCell = New XLSCell
		  // cells can have styles (fill color, fill style, text color, fomulas, vlalues, etc)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 11
		  c.value = text_param
		  c.type = XLSCell.typeString
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function createTitleDatePeriode() As XLSRow
		  Dim r as XLSRow =  new XLSRow
		  Dim c as XLSCell
		  
		  c = addTitleCell("Compagnie")
		  r.AppendCell c
		  c = addTitleCell("#Projet")
		  r.AppendCell c
		  c = addTitleCell("Description")
		  r.AppendCell c
		  c = addTitleCell("Date période")
		  r.AppendCell c
		  c = addTitleCell("Date du jour")
		  r.AppendCell c
		  c = addTitleCell("Nas")
		  r.AppendCell c
		  c = addTitleCell("Nom")
		  r.AppendCell c
		  c = addTitleCell("Prénom")
		  r.AppendCell c
		  c = addTitleCell("Salaire période")
		  r.AppendCell c
		  c = addTitleCell("Salaire contrat")
		  r.AppendCell c
		  c = addTitleCell("Part employeur calculée")
		  r.AppendCell c
		  c = addTitleCell("Cnesst employeur calculé")
		  r.AppendCell c
		  c = addTitleCell("Taux Cnesst")
		  r.AppendCell c
		  c = addTitleCell("Sal+Das+CNESST")
		  r.AppendCell c
		  c = addTitleCell("Applicable")
		  r.AppendCell c
		  c = addTitleCell("Part Employeur")
		  r.AppendCell c
		  c = addTitleCell("AvImPosFed")
		  r.AppendCell c
		  
		  MainWindowEditField.text = "Titre écrit"+ chr(13)
		  Return r
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function createTitleProjet() As XLSRow
		  Dim r as XLSRow =  new XLSRow
		  Dim c as XLSCell
		  
		  c = addTitleCell("Compagnie")
		  r.AppendCell c
		  c = addTitleCell("#Projet")
		  r.AppendCell c
		  c = addTitleCell("Description")
		  r.AppendCell c
		  c = addTitleCell("Date période")
		  r.AppendCell c
		  c = addTitleCell("Date du jour")
		  r.AppendCell c
		  c = addTitleCell("Nas")
		  r.AppendCell c
		  c = addTitleCell("Nom")
		  r.AppendCell c
		  c = addTitleCell("Prénom")
		  r.AppendCell c
		  c = addTitleCell("Salaire période")
		  r.AppendCell c
		  c = addTitleCell("Salaire contrat")
		  r.AppendCell c
		  c = addTitleCell("Part employeur calculée")
		  r.AppendCell c
		  c = addTitleCell("Cnesst employeur calculé")
		  r.AppendCell c
		  c = addTitleCell("Sal+Das+CNESST")
		  r.AppendCell c
		  
		  MainWindowEditField.text = "Titre écrit"+ chr(13)
		  Return r
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub determineEnvironnement()
		  //Initialize and verify if on Production or Test
		  //We'll use an INI settings file to read the environment value
		  
		  #if DebugBuild then
		    UserSettings = GetFolderItem( "").Parent.Child("ProdOuTest.ini")
		  #else
		    UserSettings = GetFolderItem("ProdOuTest.ini")
		  #Endif
		  
		  Dim folderItem As FolderItem = UserSettings
		  OpenINI(UserSettings)
		  
		  environnementProp = INI_File.Get("ProductionOuTest","Env","")
		  
		  host = HOSTPROD
		  If environnementProp = "Preproduction" Then  // Nous sommes en préproduction
		    host = HOSTPREPRODUCTION
		  ElseIf environnementProp = "Test" Then  // Nous sommes en test
		    host = HOSTTEST
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function GMTextConversion(tempString As String) As String
		  Dim c as TextConverter
		  c=GetTextConverter(GetTextEncoding(&h500), GetTextEncoding(0))
		  return c.convert(tempString)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub listeDatePeriodePaie(compagnie_param As String)
		  If bdGOP <> Nil Then
		    bdGOP.Close
		    bdGOP = Nil
		  End If
		  
		  Dim message,strSQL As String
		  message = ouvertureBD(CompagniePopupMenu.Text)
		  
		  DatePeriodePaiePopupMenu.DeleteAllRows
		  // Lecture du réalisé pour les dépenses par code de Grand Livre
		  strSQL = _
		  "SELECT realisemocont_date_periode " + _
		  "  FROM realisemocont" + _
		  "  GROUP BY realisemocont_date_periode" + _
		  "  ORDER BY  realisemocont_date_periode DESC"
		  
		  Dim datePeriodeRS As RecordSet = bdGop.SQLSelect(strSQL)
		  If datePeriodeRS = Nil Then Exit
		  While Not  datePeriodeRS.EOF
		    DatePeriodePaiePopupMenu.AddRow(datePeriodeRS.Field("realisemocont_date_periode").StringValue)
		    datePeriodeRS.MoveNext
		  Wend
		  DatePeriodePaiePopupMenu.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub listeProjet(compagnie_param As String)
		  If bdGOP <> Nil Then
		    bdGOP.Close
		    bdGOP = Nil
		  End If
		  
		  Dim message,strSQL As String
		  message = ouvertureBD(CompagniePopupMenu.Text)
		  
		  ProjetPopupMenu.DeleteAllRows
		  // Lecture du réalisé pour les dépenses par code de Grand Livre
		  strSQL = _
		  "SELECT realisemocont_projet_no " + _
		  "  FROM realisemocont" + _
		  "  GROUP BY realisemocont_projet_no" + _
		  "  ORDER BY  realisemocont_projet_no DESC"
		  
		  Dim projetRS As RecordSet = bdGop.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  While Not  projetRS.EOF
		    ProjetPopupMenu.AddRow(projetRS.Field("realisemocont_projet_no").StringValue)
		    projetRS.MoveNext
		  Wend
		  ProjetPopupMenu.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub OpenINI(strFile as FolderItem)
		  INI_File=new INISettings(strFile)
		  INI_File.Load
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ouvertureBD(compagnie_param As String) As String
		  Dim message As String = "Succes"
		  
		  // Ouverture base de données de la compagnie
		  bdGOP = New PostgreSQLDatabase
		  bdGOP.Host = host
		  bdGOP.UserName = GenControlModule.USER
		  bdGOP.Password = GenControlModule.PASSWORD
		  bdGOP.DatabaseName = compagnie_param
		  
		  If Not (bdGOP.Connect) Then
		    message = GMTextConversion(bdGOP.ErrorMessage)
		  Else
		    bdGOP.SQLExecute("SET search_path TO dashboard, equip, dbglobal, portail, locking, audit, public;")
		    If  bdGOP .Error Then
		      message = GMTextConversion(bdGOP.ErrorMessage)
		    End If
		  End If
		  
		  Return message
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function retenuExtr(nas_param As String, date_periode_param As String) As Dictionary
		  Dim strSQL As String = ""
		  Dim retenueDict As New Dictionary 
		  
		  // Lecture de l'estimé pour le revenu total
		  strSQL = "SELECT  realisemoret_applicable As Applicable, realisemoret_partemployeur As PartEmployeur, realisemoret_avimposfed As AvImPosFed " + _
		  "        FROM realisemoret " + _ 
		  "       WHERE realisemoret_nas = '" + nas_param + "'"  + _
		  "          AND realisemoret_date_periode = '" + date_periode_param + "'" 
		  Dim retenueRS As RecordSet = bdGOP.SQLSelect(strSQL)
		  If retenueRS = Nil Then Exit
		  retenueRS.MoveFirst
		  retenueDict.Value("Applicable") = retenueRS.Field("Applicable").DoubleValue
		  retenueDict.Value("PartEmployeur") = retenueRS.Field("PartEmployeur").DoubleValue
		  retenueDict.Value("AvImPosFed") = retenueRS.Field("AvImPosFed").DoubleValue
		  retenueRS.Close
		  retenueRS = Nil
		  
		  
		  Return retenueDict
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		bdGOP As PostgreSQLDatabase
	#tag EndProperty

	#tag Property, Flags = &h0
		environnementProp As String
	#tag EndProperty

	#tag Property, Flags = &h0
		host As String
	#tag EndProperty

	#tag Property, Flags = &h0
		INI_File As INISettings
	#tag EndProperty

	#tag Property, Flags = &h0
		UserSettings As FolderItem
	#tag EndProperty


#tag EndWindowCode

#tag Events CompagniePopupMenu
	#tag Event
		Sub Change()
		  listeDatePeriodePaie(CompagniePopupMenu.Text)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ProjetPushButton
	#tag Event
		Sub Action()
		  createChiffrierCompagnie(CompagniePopupMenu.Text,"Projet")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DatePeriodePushButton
	#tag Event
		Sub Action()
		  createChiffrierCompagnie(CompagniePopupMenu.Text, "DatePeriode")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="environnementProp"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="host"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
